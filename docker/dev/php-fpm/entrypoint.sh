#!/usr/bin/env sh

composer install
bin/console cache:clear
bin/console cache:warmup
php-fpm --nodaemonize

