user nginx;

worker_processes 2;

error_log /var/log/nginx/error.log warn;
pid /var/run/nginx.pid;

events {
    worker_connections 2048;
}

http {
    include /etc/nginx/mime.types;
    default_type application/octet-stream;

    # TODO check format [02/Nov/2018:17:47:57 +0000] x.x.x.x "GET /myapp HTTP/1.1" 200 0.616
    log_format main '[$time_local] $remote_addr  $status "$request" $body_bytes_sent "$http_referer" "$http_user_agent" "$http_x_forwarded_for"';

    access_log /var/log/nginx/access.log main;

    fastcgi_buffers 8 16k;
    fastcgi_buffer_size 32k;
    fastcgi_connect_timeout 300;
    fastcgi_send_timeout 300;
    fastcgi_read_timeout 300;

    sendfile on;
    tcp_nopush on;
    tcp_nodelay on;
    keepalive_timeout 60;
    server_tokens off;

    # Add X-XSS-Protection for HTML documents.
    map $sent_http_content_type $x_xss_protection {
        ~*text/html "1; mode=block";
    }
    # Add X-Frame-Options for HTML documents.
    map $sent_http_content_type $x_frame_options {
        ~*text/html DENY;
    }
    # Add Content-Security-Policy for HTML documents.
    map $sent_http_content_type $content_security_policy {
        ~*text/(html|javascript)|application/pdf|xml "default-src 'self'; base-uri 'none'; form-action 'self'; frame-ancestors 'none'; upgrade-insecure-requests";
    }
    # Add Referrer-Policy for HTML documents.
    map $sent_http_content_type $referrer_policy {
        ~*text/(css|html|javascript)|application\/pdf|xml "strict-origin-when-cross-origin";
    }

    # https://github.com/h5bp/server-configs-nginx/blob/master/h5bp/security/content-security-policy.conf
    #add_header Content-Security-Policy $content_security_policy always;
    # https://github.com/h5bp/server-configs-nginx/blob/master/h5bp/security/referrer-policy.conf
    add_header Referrer-Policy $referrer_policy always;
    # https://github.com/h5bp/server-configs-nginx/blob/master/h5bp/security/strict-transport-security.conf
    add_header Strict-Transport-Security 'max-age=31536000; includeSubDomains; preload' always;
    # https://github.com/h5bp/server-configs-nginx/blob/master/h5bp/security/x-content-type-options.conf
    add_header X-Content-Type-Options nosniff always;
    # https://github.com/h5bp/server-configs-nginx/blob/master/h5bp/security/x-frame-options.conf
    add_header X-Frame-Options $x_frame_options always;
    # https://github.com/h5bp/server-configs-nginx/blob/master/h5bp/security/x-xss-protection.conf
    add_header X-XSS-Protection $x_xss_protection always;

    types_hash_max_size 2048;
    gzip on;

    include /etc/nginx/conf.d/*.conf;
}